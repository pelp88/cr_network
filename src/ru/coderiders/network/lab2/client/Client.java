package ru.coderiders.network.lab2.client;

import ru.coderiders.network.lab2.classes.TCPConnection;
import ru.coderiders.network.lab2.classes.TCPConnectionListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client implements TCPConnectionListener {
    private TCPConnection connection;

    public Client(){
        Scanner scanner = new Scanner(System.in);
        makeConnection();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("Client is shutting down, bye!")));
        System.out.println("Session started, to exit write /exit or press Ctrl-C");
        while (true) {
            var msg = scanner.next();
            if ("/exit".equals(msg)) {
                Runtime.getRuntime().exit(0);
            } else {
                sendMessage(msg);
            }
        }
    }

    private void makeConnection(){
        try {
            String ipAddress = getHostIpAddress();
            int port = 1000;
            connection = new TCPConnection(this, ipAddress, port);
            System.out.println(connection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getHostIpAddress() throws IOException {
        try(final Socket socket = new Socket()){
            socket.connect(new InetSocketAddress("google.com", 80));
            var addr = socket.getLocalAddress().getHostAddress();
            System.out.println("Got host IP addr: " + addr);
            return addr;
        }
    }

    public void sendMessage(String message) {
        connection.sendString(message);
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        System.out.println("Connection established!");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        System.out.println(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        System.out.println("Connection closed, terminating session...");
        Runtime.getRuntime().exit(0);
    }

    @Override
    public void onException(TCPConnection tcpConnection, IOException e) {
        System.out.println("Caught connection exception: " + e);
        System.out.println("Terminating session...");
        Runtime.getRuntime().exit(0);
    }
}
