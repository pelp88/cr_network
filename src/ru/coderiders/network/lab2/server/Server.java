package ru.coderiders.network.lab2.server;

import ru.coderiders.network.lab2.classes.TCPConnection;
import ru.coderiders.network.lab2.classes.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

@SuppressWarnings("InfiniteLoopStatement")
public class Server implements TCPConnectionListener {
    private TCPConnection connection;

    public Server() {
        System.out.println("Server running...");
        try (ServerSocket serverSocket = new ServerSocket(1000)) {
            while (true) {
                try {
                    new TCPConnection(this, serverSocket.accept());
                } catch (IOException e) {
                    System.out.println("TCPConnection exception: " + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        connection = tcpConnection;
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        sendBroadcastMessage(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        connection = null;
    }

    @Override
    public void onException(TCPConnection tcpConnection, IOException e) {
        System.out.println("TCPConnection exception: " + e);
    }

    private void sendBroadcastMessage(String value) {
        System.out.println(value);
        connection.sendString("Server received message: " + value +
                " - " + java.util.Calendar.getInstance().getTime());
    }
}
