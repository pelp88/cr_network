package ru.coderiders.network.lab3.client;

import ru.coderiders.network.lab3.classes.TCPConnection;
import ru.coderiders.network.lab3.classes.TCPConnectionListener;

import java.net.*;
import java.io.IOException;
import java.util.Scanner;

public class ChatClient implements TCPConnectionListener {
    private TCPConnection connection;
    private final String nickName;

    public ChatClient(){
        Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("Client is shutting down, bye!")));
        System.out.println("Enter nickname: ");
        Scanner scanner = new Scanner(System.in);
        nickName = scanner.next();
        try {
            makeConnection();
        } catch (IOException e) {
            System.out.println("Error occurred: " + e);
            Runtime.getRuntime().exit(0);
        }
        System.out.println("Entering chat, to exit write /exit or press Ctrl-C\n\n");
        while (true) {
            var msg = scanner.nextLine();
            if ("/exit".equals(msg)) {
                Runtime.getRuntime().exit(0);
            } else if (!"".equals(msg)){
                sendMessage(msg);
            }
        }
    }

    private void makeConnection() throws IOException {
        String ipAddress = getHostIpAddress();
        int port = 1000;
        connection = new TCPConnection(this, ipAddress, port);
        System.out.println(connection);
    }

    private String getHostIpAddress() throws IOException {
        try(final Socket socket = new Socket()){
            socket.connect(new InetSocketAddress("google.com", 80));
            var addr = socket.getLocalAddress().getHostAddress();
            System.out.println("Got host IP addr: " + addr);
            return addr;
        }
    }

    public void sendMessage(String message) {
        connection.sendString(nickName + ": " + message);
    }

    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        System.out.println("Connection established!");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        System.out.println(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        System.out.println("Connection closed, terminating session...");
        Runtime.getRuntime().exit(0);
    }

    @Override
    public void onException(TCPConnection tcpConnection, IOException e) {
        System.out.println("Caught connection exception: " + e);
        System.out.println("Terminating session...");
        Runtime.getRuntime().exit(0);
    }
}
